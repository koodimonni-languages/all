# Translation of Development in Bosnian
# This file is distributed under the same license as the Development package.
msgid ""
msgstr ""
"PO-Revision-Date: 2015-04-18 02:35:36+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: GlotPress/1.0-alpha-1000\n"
"Project-Id-Version: Development\n"

#: views/notice.php:90
msgid "The key you entered could not be verified."
msgstr "Ključ koji ste upisali nije mogao biti verifikovan."

#: views/notice.php:91
msgid "The connection to akismet.com cannot be established. Please refer to <a href=\"%s\" target=\"_blank\">our guide about firewalls</a> and check your server configuration."
msgstr "Veza sa akismet.com nije mogla biti uspostavljena. Molimo vas da pogledate <a href=\"%s\" target=\"_blank\">naš vodič o firewallima</a>, a potom provjerite konfiguraciju vašeg servera."

#: views/config.php:76
msgid "SSL Status"
msgstr "SSL status"

#: views/config.php:83 views/config.php:86
msgid "Disabled."
msgstr "Onemogućeno."

#: views/config.php:86
msgid "Your Web server cannot make SSL requests; contact your Web host and ask them to add support for SSL requests."
msgstr "Vaš web server ne može kreirati SSL zahtjeve; kontaktirajte vaš web hosting i pitajte ih da dodaju podršku za SSL zahtjeve."

#: views/config.php:92
msgid "Temporarily disabled."
msgstr "Privremeno onemogućeno."

#: views/config.php:92
msgid "Akismet encountered a problem with a previous SSL request and disabled it temporarily. It will begin using SSL for requests again shortly."
msgstr "Akismet je naišao na problem prilikom prethodnog SSL zahtjeva i privremeno ga je onemogućio. Za par trenutaka ponovo će početi sa korištenjem SSL-a za zahtjeve."

#: views/config.php:95
msgid "Enabled."
msgstr "Omogućeno."

#: views/config.php:95
msgid "All systems functional."
msgstr "Svi sistemi funkcionišu."

#: views/notice.php:97
msgid "Your Pro subscription allows the use of Akismet on only one site. Please <a href=\"http://docs.akismet.com/billing/add-more-sites/\">purchase additional Pro subscriptions</a> or upgrade to an Enterprise subscription that allows the use of Akismet on unlimited sites.<br /><br />If you have any questions, please get in touch with our support team."
msgstr "Vaša Pro pretplata vam omogućava da koristite Akismet samo na jednoj stranici. Molimo vas da <a href=\"http://docs.akismet.com/billing/add-more-sites/\">kupite dodatne Pro pretplate</a> ili izvršite nadogradnju na Enterprise pretplatu koja vam omogućava korištenje Akismeta na neograničenom broju stranica.<br /><br />Ako imate dodatnih pitanja, molimo vas da kontaktirate naš tim za podršku."

#: views/config.php:127
msgid "Spam in the <a href=\"%1$s\">spam folder</a> older than 1 day is deleted automatically."
msgid_plural "Spam in the <a href=\"%1$s\">spam folder</a> older than %2$d days is deleted automatically."
msgstr[0] "Spam u <a href=\"%1$s\">spam folderu</a> stariji od %2$d dana se briše automatski."
msgstr[1] "Spam u <a href=\"%1$s\">spam folderu</a> stariji od %2$d dana se briše automatski."
msgstr[2] "Spam u <a href=\"%1$s\">spam folderu</a> stariji od %2$d dana se briše automatski."

#. Plugin URI of the plugin/theme
msgid "http://akismet.com/"
msgstr "http://akismet.com/"

#. Description of the plugin/theme
msgid "Used by millions, Akismet is quite possibly the best way in the world to <strong>protect your blog from comment and trackback spam</strong>. It keeps your site protected from spam even while you sleep. To get started: 1) Click the \"Activate\" link to the left of this description, 2) <a href=\"http://akismet.com/get/\">Sign up for an Akismet API key</a>, and 3) Go to your Akismet configuration page, and save your API key."
msgstr "Korišten od strane miliona, Akismet je vrlo vjerovatno najbolji način <strong>zaštite vašeg bloga o spama u komentarima i pozivima na svijetu</strong>. Vaša stranica je branjena od spama čak i kada spavate. Da započnete: 1) Kliknite na dugme \"Aktiviraj\" lijevo od ovog opisa, 2) <a href=\"http://akismet.com/get/\">Registrujte vaš Akismet API ključ</a>, i 3) Otvorite vašu Akismet konfiguraciju i tamo sačuvajte vaš API ključ."

#: class.akismet-admin.php:700
msgid "Please check your <a href=\"%s\">Akismet configuration</a> and contact your web host if problems persist."
msgstr "Molimo vas da provjerite vašu <a href=\"%s\">Akismet konfiguraciju</a> i da kontaktirate vaš web hosting ako se problem nastavi dešavati."

#: views/notice.php:21
msgid "Some comments have not yet been checked for spam by Akismet. They have been temporarily held for moderation and will automatically be rechecked later."
msgstr "Neki komentari još nisu provjereni od strane Akismeta. Oni su privremeno preusmjereni na moderaciju i bit će automatski ponovo provjereni kasnije."

#: views/notice.php:43
msgid "Your web host or server administrator has disabled PHP&#8217;s <code>gethostbynamel</code> functions.  <strong>Akismet cannot work correctly until this is fixed.</strong>  Please contact your web host or firewall administrator and give them <a href=\"%s\" target=\"_blank\">this information about Akismet&#8217;s system requirements</a>."
msgstr "Vaš web hosting ili administrator servera je onemogućio PHP <code>gethostbynamel</code> funkcije. <strong>Akismet neće raditi ispravno sve dok se ovo ne popravi.</strong> Molimo vas da kontaktirate vaš web hosting ili firewall administratora i date im <a href=\"%s\" target=\"_blank\">ovu informaciju o zahtjevima za funkcionisanje Akismeta</a>."

#: views/start.php:77
msgid "Activate Akismet"
msgstr "Aktiviraj Akismet"

#: views/start.php:64 views/start.php:85
msgid "If you already know your API key."
msgstr "Ako već posjedujete vaš API ključ."

#: views/start.php:78
msgid "Log in or create an account to get your API key."
msgstr "Prijavite se ili kreirajte račun da dobijete vaš API ključ."

#: views/start.php:80
msgid "Get your API key"
msgstr "Nabavi API ključ"

#: views/start.php:22
msgid "Your subscription for %s is cancelled"
msgstr "Vaša pretplata na %s je otkazana"

#: views/start.php:28
msgid "Reactivate Akismet"
msgstr "Reaktiviraj Akismet"

#: views/config.php:182
msgid "Cancelled"
msgstr "Otkazano"

#: views/config.php:184
msgid "Suspended"
msgstr "Suspendovano"

#: views/config.php:186
msgid "Missing"
msgstr "Nedostaje"

#: views/config.php:188
msgid "No Subscription Found"
msgstr "Pretplata nije pronađena"

#: views/config.php:190
msgid "Active"
msgstr "Aktivno"

#: views/notice.php:72
msgid "There is a problem with your key."
msgstr "Postoji problem sa vašim ključem."

#: views/notice.php:78
msgid "Since 2012, Akismet began using subscriptions for all accounts (even free ones). It looks like a subscription has not been assigned to your account, and we’d appreciate it if you’d <a href=\"%s\" target=\"_blank\">sign into your account</a> and choose one. Please <a href=\"%s\" target=\"_blank\">contact our support team</a> with any questions."
msgstr "Od 2012. godine Akismet je aktivirao pretplatu na svim računima (čak i na besplatnim). Izgleda da pretplata nije dodijeljena za vaš račun, pa bismo vam bili zahvalni ako biste sada <a href=\"%s\" target=\"_blank\">izvršili prijavu u vaš račun</a> i odabrali jednu od ponuđenih. Molimo vas da <a href=\"%s\" target=\"_blank\">kontaktirate našu podršku</a> ako imate bilo kakvih pitanja ili nedoumica."

#: class.akismet-admin.php:196 views/config.php:114
msgid "Strictness"
msgstr "Strogoća"

#: class.akismet-admin.php:196
msgid "Choose to either discard the worst spam automatically or to always put all spam in spam folder."
msgstr "Odaberite da li želite automatski odstraniti najgore spam poruke ili da li ih želite sve smjestiti u spam folder."

#: views/config.php:118
msgid "Silently discard the worst and most pervasive spam so I never see it."
msgstr "Odstrani najgore i najnametljivije vrsta spama jer ne želim vidjeti te vrste poruka."

#: views/config.php:119
msgid "Always put spam in the Spam folder for review."
msgstr "Sve spam poruke smjesti u spam folder kako bi ih bilo moguće pregledati."

#: class.akismet-admin.php:195 views/config.php:105
msgid "Comments"
msgstr "Komentari"

#: views/notice.php:77
msgid "Your subscription is missing."
msgstr "Nemate aktivnu pretplatu."

#: views/notice.php:96
msgid "You're using your Akismet key on more sites than your Pro subscription allows."
msgstr "Vaš Akismet ključ koristite na više stranica nego što vam to dozvoljava vaša Pro pretplata."

#: views/notice.php:99
msgid "You're using Akismet on far too many sites for your Pro subscription."
msgstr "Koristite Akismet na više stranica nego što je to predviđeno vašom Pro pretplatom."

#: views/notice.php:100
msgid "To continue your service, <a href=\"%s\" target=\"_blank\">upgrade to an Enterprise subscription</a>, which covers an unlimited number of sites. Please <a href=\"%s\" target=\"_blank\">contact our support team</a> with any questions."
msgstr "Da biste nastavili sa korištenjem ovog servisa, <a href=\"%s\" target=\"_blank\">izvršite nadogradnju na Enterprise pretplatu</a>, u kojoj je moguće koristiti servis na neograničenom broju stranica. Molimo vas da  <a href=\"%s\" target=\"_blank\">kontaktirate naš tim za podršku</a> ako imate bilo kakvih pitanja."

#: views/notice.php:20
msgid "Akismet has detected a problem."
msgstr "Akismet je otkrio problem."

#: views/notice.php:27
msgid "Akismet %s requires WordPress 3.0 or higher."
msgstr "Akismet %s zahtjeva WordPress 3.0 ili noviji."

#: views/notice.php:30
msgid "Akismet Error Code: %s"
msgstr "Akismet greška broj: %s"

#. translators: the placeholder is a clickable URL that leads to more
#. information regarding an error code.
#: views/notice.php:35
msgid "For more information: %s"
msgstr "Za više informacija: %s"

#: views/notice.php:42
msgid "Network functions are disabled."
msgstr "Mrežne funkcije su onemogućene."

#: views/notice.php:47
msgid "We can&#8217;t connect to your site."
msgstr "Ne možemo se povezati sa vašom stranicom."

#: views/notice.php:48
msgid "Your firewall may be blocking us. Please contact your host and refer to <a href=\"%s\" target=\"_blank\">our guide about firewalls</a>."
msgstr "Vaš firewall bi nas mogao blokirati. Molimo vas da kontaktirate vaš hosting i obavijestite ih o <a href=\"%s\" target=\"_blank\">našem uputstvu vezanom za firewalle</a>."

#: views/notice.php:52
msgid "Please update your payment details."
msgstr "Molimo vas da ažurirate vaše informacije o plaćanju."

#: views/notice.php:53
msgid "We cannot process your transaction. Please contact your bank for assistance, and <a href=\"%s\" target=\"_blank\">update your payment details</a>."
msgstr "Ne možemo procesirati vašu transakciju. Molimo vas da kontaktirate vašu banku i da <a href=\"%s\" target=\"_blank\">provjerite vaše detalje o plaćanju</a>."

#: views/notice.php:57
msgid "Your subscription is cancelled."
msgstr "Vaša pretplata je otkazana."

#: views/notice.php:58
msgid "Please visit the <a href=\"%s\" target=\"_blank\">Akismet account page</a> to reactivate your subscription."
msgstr "Molimo vas da posjetite <a href=\"%s\" target=\"_blank\">Akismet stranicu sa vašim računom</a> da biste reaktivirali vašu pretplatu."

#: views/notice.php:62
msgid "Your subscription is suspended."
msgstr "Vaša pretplata je suspendovana."

#: views/notice.php:63 views/notice.php:73
msgid "Please contact <a href=\"%s\" target=\"_blank\">Akismet support</a> for assistance."
msgstr "Molimo vas da kontaktirate <a href=\"%s\" target=\"_blank\">Akismet podršku</a> i zatražite pomoć."

#: views/notice.php:68
msgid "You can help us fight spam and upgrade your account by <a href=\"%s\" target=\"_blank\">contributing a token amount</a>."
msgstr "Možete nam pomoći u borbi protiv spama tako što ćete nadograditi vaš račun <a href=\"%s\" target=\"_blank\">plaćanjem određenog simboličnog novčanog iznosa</a>."

#: views/notice.php:82
msgid "Your Akismet account has been successfully set up and activated. Happy blogging!"
msgstr "Vaš Akismet račun je uspješno postavljen i aktiviran! Sretno bloganje!"

#: views/notice.php:86
msgid "The key you entered is invalid. Please double-check it."
msgstr "Ključ koji ste unijeli je neispravan. Molimo vas da ga provjerite opet."

#: views/start.php:40 views/start.php:74
msgid "Akismet eliminates the comment and trackback spam you get on your site. To setup Akismet, select one of the options below."
msgstr "Akismet eliminira spam u komentarima i povratnim pozivima na vašoj stranici. Da izvršite postavku Akismeta, odaberite neku od opcija ispod."

#: views/start.php:7 views/start.php:21 views/start.php:34 views/start.php:43
msgid "Connected via Jetpack"
msgstr "Povezan preko Jetpacka"

#: views/start.php:50
msgid "Use this Akismet account"
msgstr "Koristi ovaj Akismet račun"

#: views/start.php:56
msgid "Create a new API key with a different email address"
msgstr "Kreiraj novi API ključ koristeći drugu email adresu"

#: views/start.php:57
msgid "Use this option if you want to setup a new Akismet account."
msgstr "Koristite ovu opciju ako želite napraviti novi Akismet račun."

#: views/start.php:59
msgid "Register a different email address"
msgstr "Registruj drugu email adresu"

#: views/start.php:63 views/start.php:84
msgid "Manually enter an API key"
msgstr "Ručno upiši API ključ"

#: views/start.php:70 views/start.php:91
msgid "Use this key"
msgstr "Koristi ovaj ključ"

#: views/start.php:4
msgid "Akismet eliminates the comment and trackback spam you get on your site. Register your email address below to get started."
msgstr "Akismet eliminira spam u komentarima i povratnim pozivima na vašoj stranici. Da započnete sa korištenjem, izvršite registraciju vaše email adresu ispod."

#: views/start.php:14
msgid "Register Akismet"
msgstr "Registruj Akismet"

#: views/start.php:18 views/start.php:32
msgid "Akismet eliminates the comment and trackback spam you get on your site."
msgstr "Akismet eliminira spam u komentarima i povratnim pozivima na vašoj stranici."

#: views/start.php:35
msgid "Your subscription for %s is suspended"
msgstr "Vaša pretplata za %s je suspendovana"

#: views/start.php:36
msgid "No worries! Get in touch and we&#8217;ll help sort this out."
msgstr "Nemojte se brinuti! Kontaktirajte nas i mi ćemo vam pomoći da riješite ovaj problem."

#: views/start.php:37
msgid "Contact Akismet support"
msgstr "Kontaktiraj Akismet podršku"

#: views/config.php:117 views/strict.php:2 views/strict.php:3
msgid "Akismet anti-spam strictness"
msgstr "Strogoća Akismet anti-spam filtera"

#: views/strict.php:4
msgid "Strict: silently discard the worst and most pervasive spam."
msgstr "Strogo: automatski odbaci najgore vrste spama."

#: views/strict.php:5
msgid "Safe: always put spam in the Spam folder for review."
msgstr "Sigurno: spam uvijek smjesti u spam folder kako bi ga kasnije bilo moguće pregledati."

#. Author of the plugin/theme
msgid "Automattic"
msgstr "Automattic"

#. Author URI of the plugin/theme
msgid "http://automattic.com/wordpress-plugins/"
msgstr "http://automattic.com/wordpress-plugins/"

#: class.akismet-admin.php:53
msgid "Comment History"
msgstr "Historija komentara"

#. #-#-#-#-#  akismet.pot (Akismet 3.1.2a1)  #-#-#-#-#
#. Plugin Name of the plugin/theme
#: class.akismet-admin.php:76 class.akismet-admin.php:78
#: class.akismet-admin.php:757 views/config.php:3
msgid "Akismet"
msgstr "Akismet"

#: class.akismet-admin.php:104
msgid "Remove this URL"
msgstr "Ukloni ovaj URL"

#: class.akismet-admin.php:105
msgid "Removing..."
msgstr "Uklanjam..."

#: class.akismet-admin.php:106
msgid "URL removed"
msgstr "URL je uklonjen"

#: class.akismet-admin.php:107
msgid "(undo)"
msgstr "(poništi)"

#: class.akismet-admin.php:108
msgid "Re-adding..."
msgstr "Ponovo dodajem..."

#: class.akismet-admin.php:129 class.akismet-admin.php:167
#: class.akismet-admin.php:180
msgid "Overview"
msgstr "Pregled"

#: class.akismet-admin.php:131 class.akismet-admin.php:142
#: class.akismet-admin.php:153
msgid "Akismet Setup"
msgstr "Postavka Akismeta"

#: class.akismet-admin.php:132 class.akismet-admin.php:170
#: class.akismet-admin.php:183
msgid "Akismet filters out your comment and trackback spam for you, so you can focus on more important things."
msgstr "Akismet umjesto vas eliminira spam u komentarima i povratnim pozivima kako biste se vi mogli fokusirati na važnije stvari."

#: class.akismet-admin.php:133
msgid "On this page, you are able to setup the Akismet plugin."
msgstr "Na ovo stranici možete izvršiti postavku vašeg Akismet plugina."

#: class.akismet-admin.php:140
msgid "New to Akismet"
msgstr "Novi korisnik Akismeta"

#: class.akismet-admin.php:143
msgid "You need to enter an API key to activate the Akismet service on your site."
msgstr "Morate upisati API ključ da biste aktivirali Akismet servis na vašoj stranici."

#: class.akismet-admin.php:144
msgid "Signup for an account on %s to get an API Key."
msgstr "Registrujte račun na %s da dobijete API ključ."

#: class.akismet-admin.php:151
msgid "Enter an API Key"
msgstr "Upište API ključ"

#: class.akismet-admin.php:154
msgid "If you already have an API key"
msgstr "Ako već posjedujete API ključ"

#: class.akismet-admin.php:156
msgid "Copy and paste the API key into the text field."
msgstr "Kopirajte i umetnite API ključ u tekstualno polje."

#: class.akismet-admin.php:157
msgid "Click the Use this Key button."
msgstr "Kliknite na dugme Koristi ovaj ključ."

#: class.akismet-admin.php:169 views/stats.php:2
msgid "Akismet Stats"
msgstr "Akismet statistika"

#: class.akismet-admin.php:171
msgid "On this page, you are able to view stats on spam filtered on your site."
msgstr "Na ovoj stranici možete pregledati statistiku vezanu za filtrirani spam na vašoj stranici."

#: class.akismet-admin.php:182 class.akismet-admin.php:193
#: class.akismet-admin.php:205
msgid "Akismet Configuration"
msgstr "Konfiguracija Akismeta"

#: class.akismet-admin.php:184
msgid "On this page, you are able to enter/remove an API key, view account information and view spam stats."
msgstr "Na ovoj stranici možete upisati/ukloniti API ključ i pregledati informacije o vašem računu i spam statistici."

#: class.akismet-admin.php:69 class.akismet-admin.php:191
#: class.akismet-admin.php:508 views/config.php:60 views/stats.php:2
msgid "Settings"
msgstr "Postavke"

#: class.akismet-admin.php:194 views/config.php:67
msgid "API Key"
msgstr "API ključ"

#: class.akismet-admin.php:194
msgid "Enter/remove an API key."
msgstr "Upišite/uklonite API ključ."

#: views/config.php:109
msgid "Show the number of approved comments beside each comment author"
msgstr "Prikaži broj odobrenih komentara pored imena svakog autora"

#: class.akismet-admin.php:195
msgid "Show the number of approved comments beside each comment author in the comments list page."
msgstr "Prikazuje broj odobrenih komentara pored imena svakog od autora u listi komentara."

#: class.akismet-admin.php:203 views/config.php:165
msgid "Account"
msgstr "Račun"

#: class.akismet-admin.php:206 views/config.php:170
msgid "Subscription Type"
msgstr "Vrsta pretplate"

#: class.akismet-admin.php:206
msgid "The Akismet subscription plan"
msgstr "Akismet plan pretplate"

#: class.akismet-admin.php:207 views/config.php:177
msgid "Status"
msgstr "Status"

#: class.akismet-admin.php:207
msgid "The subscription status - active, cancelled or suspended"
msgstr "Status pretplate - aktivna, otkazana ili suspendovana"

#: class.akismet-admin.php:215
msgid "For more information:"
msgstr "Za više informacija:"

#: class.akismet-admin.php:216
msgid "Akismet FAQ"
msgstr "Akismet FAQ"

#: class.akismet-admin.php:217
msgid "Akismet Support"
msgstr "Akismet podrška"

#: class.akismet-admin.php:223
msgid "Cheatin&#8217; uh?"
msgstr "Varate?"

#: class.akismet-admin.php:282
msgctxt "comments"
msgid "Spam"
msgstr "Spam"

#: class.akismet-admin.php:284
msgid "<a href=\"%1$s\">Akismet</a> has protected your site from <a href=\"%2$s\">%3$s spam comment</a>."
msgid_plural "<a href=\"%1$s\">Akismet</a> has protected your site from <a href=\"%2$s\">%3$s spam comments</a>."
msgstr[0] "<a href=\"%1$s\">Akismet</a> je zaštitio vašu stranicu od <a href=\"%2$s\">%3$s spam komentara</a>."
msgstr[1] "<a href=\"%1$s\">Akismet</a> je zaštitio vašu stranicu od <a href=\"%2$s\">%3$s spam komentara</a>."
msgstr[2] "<a href=\"%1$s\">Akismet</a> je zaštitio vašu stranicu od <a href=\"%2$s\">%3$s spam komentara</a>."

#: class.akismet-admin.php:303
msgid "<a href=\"%1$s\">Akismet</a> has protected your site from %2$s spam comment already. "
msgid_plural "<a href=\"%1$s\">Akismet</a> has protected your site from %2$s spam comments already. "
msgstr[0] "<a href=\"%1$s\">Akismet</a> je zaštitio vašu stranicu od %2$s spam komentara. "
msgstr[1] "<a href=\"%1$s\">Akismet</a> je zaštitio vašu stranicu od %2$s spam komentara."
msgstr[2] "<a href=\"%1$s\">Akismet</a> je zaštitio vašu stranicu od %2$s spam komentara."

#: class.akismet-admin.php:309
msgid "<a href=\"%s\">Akismet</a> blocks spam from getting to your blog. "
msgstr "<a href=\"%s\">Akismet</a> blokira spam na vašem blogu. "

#: class.akismet-admin.php:314
msgid "There&#8217;s <a href=\"%2$s\">%1$s comment</a> in your spam queue right now."
msgid_plural "There are <a href=\"%2$s\">%1$s comments</a> in your spam queue right now."
msgstr[0] "Trenutno je <a href=\"%2$s\">%1$s komentar</a> u vašem spam redu."
msgstr[1] "Trenutno su <a href=\"%2$s\">%1$s komentara</a> u vašem spam redu."
msgstr[2] "Trenutno je <a href=\"%2$s\">%1$s komentara</a> u vašem spam redu."

#: class.akismet-admin.php:320
msgid "There&#8217;s nothing in your <a href='%s'>spam queue</a> at the moment."
msgstr "Trenutno se ništa ne nalazi u vašem <a href='%s'>spam redu</a>."

#: class.akismet-admin.php:340
msgid "Check for Spam"
msgstr "Provjeri ima li spama"

#: class.akismet.php:446
msgid "%1$s changed the comment status to %2$s"
msgstr "%1$s je promijenio status komentara u %2$s"

#: class.akismet-admin.php:382
msgid "Akismet re-checked and caught this comment as spam"
msgstr "Akismet je izvršio ponovnu provjeru i označio je ovaj komentar kao spam"

#: class.akismet-admin.php:388
msgid "Akismet re-checked and cleared this comment"
msgstr "Akismet je izvršio ponovnu provjeru i potvrdio je da je ovo ispravan komentar."

#: class.akismet-admin.php:392
msgid "Akismet was unable to re-check this comment (response: %s)"
msgstr "Akismet nije mogao ponovo provjeriti ovaj komentar (odgovor: %s)"

#: class.akismet-admin.php:446
msgid "Awaiting spam check"
msgstr "Čekam na spam provjeru"

#: class.akismet-admin.php:450
msgid "Flagged as spam by Akismet"
msgstr "Akismet je ovo označio kao spam"

#: class.akismet-admin.php:452
msgid "Cleared by Akismet"
msgstr "Odobrio Akismet"

#: class.akismet-admin.php:456
msgid "Flagged as spam by %s"
msgstr "Označeno kao spam od strane %s"

#: class.akismet-admin.php:458
msgid "Un-spammed by %s"
msgstr "%s je utvrdio da ovo nije spam"

#: class.akismet-admin.php:470 class.akismet-admin.php:478
msgid "View comment history"
msgstr "Pregledaj historiju komentara"

#: class.akismet-admin.php:470
msgid "History"
msgstr "Historija"

#: class.akismet-admin.php:486
msgid "%s approved"
msgid_plural "%s approved"
msgstr[0] "%s odobren"
msgstr[1] "%s odobrena"
msgstr[2] "%s odobrenih"

#: class.akismet-admin.php:499
msgid "%s ago"
msgstr "prije %s"

#: class.akismet.php:491
msgid "%s reported this comment as spam"
msgstr "%s je prijavio ovaj komentar kao spam"

#: class.akismet.php:537
msgid "%s reported this comment as not spam"
msgstr "%s je prijavio ovaj komentar kao spam"

#: class.akismet-admin.php:787
msgid "Cleaning up spam takes time."
msgstr "Čišćenje spama traje poprilično dugo."

#: class.akismet-admin.php:790
msgid "Since you joined us, Akismet has saved you %s days!"
msgstr "Od trenutka instaliranja, Akismet vam je sačuvao %s dana!"

#: class.akismet-admin.php:792
msgid "Since you joined us, Akismet has saved you %d hours!"
msgstr "Od trenutka instaliranja, Akismet vam je sačuvao %d sati!"

#: class.akismet-admin.php:794
msgid "Since you joined us, Akismet has saved you %d minutes!"
msgstr "Od trenutka instaliranja, Akismet vam je sačuvao %d minuta!"

#: class.akismet-widget.php:12
msgid "Akismet Widget"
msgstr "Akismet dodatak"

#: class.akismet-widget.php:13
msgid "Display the number of spam comments Akismet has caught"
msgstr "Prikaži broj spam komentara koje je Akismet pronašao"

#: class.akismet-widget.php:69
msgid "Spam Blocked"
msgstr "Spam blokiran"

#: class.akismet-widget.php:74
msgid "Title:"
msgstr "Naslov:"

#: class.akismet-widget.php:98
msgid "<strong class=\"count\">%1$s spam</strong> blocked by <strong>Akismet</strong>"
msgid_plural "<strong class=\"count\">%1$s spam</strong> blocked by <strong>Akismet</strong>"
msgstr[0] "<strong class=\"count\">%1$s spam komentar</strong> blokiran od strane <strong>Akismeta</strong>"
msgstr[1] "<strong class=\"count\">%1$s spam komentara</strong> blokiran od strane <strong>Akismeta</strong>"
msgstr[2] "<strong class=\"count\">%1$s spam komentara</strong> blokiran od strane <strong>Akismeta</strong>"

#: class.akismet.php:231
msgid "Akismet caught this comment as spam"
msgstr "Akismet je uhvatio ovaj komentar i označio ga kao spam"

#: class.akismet.php:233 class.akismet.php:242
msgid "Comment status was changed to %s"
msgstr "Status komentara je promjenjen u %s"

#: class.akismet.php:237
msgid "Akismet cleared this comment"
msgstr "Akismet je odobrio ovaj komentar"

#: class.akismet.php:240
msgid "Comment was caught by wp_blacklist_check"
msgstr "Komentar je uhvatio wp_blacklist_check"

#: class.akismet.php:247
msgid "Akismet was unable to check this comment (response: %s), will automatically retry again later."
msgstr "Akismet nije mogao provjeriti ovaj komentar (odgovor: %s). Ponovna automatska provjera će biti izvršena kasnije."

#: class.akismet.php:578
msgid "Akismet caught this comment as spam during an automatic retry."
msgstr "Akismet je označio ovaj komentar kao spam tokom automatskog ponovnog pokušaja."

#: class.akismet.php:580
msgid "Akismet cleared this comment during an automatic retry."
msgstr "Aksimet je očistio ovaj komentar prilikom automatskog ponovnog pokušaja."

#: class.akismet.php:991
msgid "Akismet %s requires WordPress %s or higher."
msgstr "Akismet %s zahtjeva WordPress %s ili noviji."

#: class.akismet.php:991 views/notice.php:27
msgid "Please <a href=\"%1$s\">upgrade WordPress</a> to a current version, or <a href=\"%2$s\">downgrade to version 2.4 of the Akismet plugin</a>."
msgstr "Molimo vas da <a href=\"%1$s\">nadogradite WordPress</a> na najnoviju verziju, ili <a href=\"%2$s\">da instalirate Akismet plugin verziju 2.4</a>."

#: views/config.php:12
msgid "Summaries"
msgstr "Sumarno"

#: views/config.php:18
msgid "Past six months"
msgstr "U posljednjih šest mjeseci"

#: views/config.php:20 views/config.php:25
msgid "Spam blocked"
msgstr "Blokirani spam"

#: views/config.php:23
msgid "All time"
msgstr "Ukupno"

#: views/config.php:28
msgid "Accuracy"
msgstr "Preciznost"

#: views/config.php:32
msgid "%s missed spam, %s false positive"
msgid_plural "%s missed spam, %s false positives"
msgstr[0] "%s promašenih detekcija spama, %s lažnih pozitivnih detekcija"
msgstr[1] "%s promašenih detekcija spama, %s lažnih pozitivnih detekcija"
msgstr[2] "%s promašenih detekcija spama, %s lažnih pozitivnih detekcija"

#: views/config.php:109
msgid "Show approved comments"
msgstr "Prikaži odobrene komentare"

#: views/config.php:121
msgid "Note:"
msgstr "Napomena:"

#: views/config.php:146
msgid "Disconnect this account"
msgstr "Prekini vezu s ovim računom"

#: views/config.php:152
msgid "Save Changes"
msgstr "Sačuvaj promjene"

#: views/config.php:196
msgid "Next Billing Date"
msgstr "Sljedeći datum naplate"

#: views/config.php:208
msgid "Upgrade"
msgstr "Nadogradi"

#: views/config.php:208
msgid "Change"
msgstr "Promijeni"

#: views/notice.php:11
msgid "Activate your Akismet account"
msgstr "Aktivirajte vaš Akismet račun"

#: views/notice.php:14
msgid "<strong>Almost done</strong> - activate your account and say goodbye to comment spam"
msgstr "<strong>Skoro završeno</strong> - aktivirajte vaš račun i recite zbogom spamu u komentarima"