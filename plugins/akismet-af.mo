��          |       �       �      �   T   �      9     W     m     {     �     �  7   �     �  P   �  �   >  
   3  [   >  !   �     �     �     �     �     �  ?        X  G   `   %s ago <strong>Almost done</strong> - activate your account and say goodbye to comment spam Activate your Akismet account Akismet Configuration Akismet Stats Check for Spam Save Changes Show approved comments The key you entered is invalid. Please double-check it. Title: Your Akismet account has been successfully set up and activated. Happy blogging! PO-Revision-Date: 2014-03-20 22:29:14+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/1.0-alpha-1000
Project-Id-Version: Development
 %s gelede  <strong>Amper klaar</strong> - aktiveer jou rekening en sê totsiens vir gemorskommentaar.  Aktiveer jou rekening by Akismet  Akismet Keuses  Akismet Stats  Kyk vir snert  Stoor wysigings  Wys goedgekeurde kommentaar  Die sleutel wat jy ingetik het, is ongeldig. Gaan dit weer na.  Titel:  Jou Akismet-rekening is suksesvol opgestel en geaktiveer! Lekker blog!  